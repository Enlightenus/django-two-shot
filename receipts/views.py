from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm
from django.contrib.auth.decorators import login_required


@login_required
def show_receipts(request):
    context = Receipt.objects.filter(purchaser=request.user)
    web_context = {"receipt_list": context}
    return render(request, "receipts/show_receipts.html", web_context)


@login_required
def create_view(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            new_receipt = form.save(False)
            new_receipt.purchaser = request.user
            new_receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    web_context = {"create_form": form}
    return render(request, "receipts/create.html", web_context)


@login_required
def category_list(request):
    context = ExpenseCategory.objects.filter(owner=request.user)
    web_context = {"expense_list": context}
    return render(request, "receipts/list.html", web_context)
