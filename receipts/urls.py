from django.urls import path
from receipts.views import show_receipts, create_view, category_list

urlpatterns = [
    path("", show_receipts, name="home"),
    path("create/", create_view, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
]
